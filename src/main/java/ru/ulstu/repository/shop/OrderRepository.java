package ru.ulstu.repository.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.shop.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
