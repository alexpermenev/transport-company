package ru.ulstu.model.shop;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "shop_user")
public class User extends BaseEntity {
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    private String phone;
    @NonNull
    private String address;
    @NonNull
    private String email;

    @OneToMany(mappedBy = "user")
    private Set<Cart> carts;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Cart> getCarts() {
        return carts;
    }
}
