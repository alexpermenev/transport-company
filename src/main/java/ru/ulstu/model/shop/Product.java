package ru.ulstu.model.shop;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "product_shop")
public class Product extends BaseEntity {
    @NonNull
    private String title;
    @NonNull
    private Integer price;
    @NonNull
    private String description;

    @OneToMany(mappedBy = "product")
    private Set<Cart> carts;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Cart> getCarts() {
        return carts;
    }
}
