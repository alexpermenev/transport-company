package ru.ulstu.model.transport.company;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;
import ru.ulstu.model.shop.Order;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "deliver", indexes = {
        @Index(name = "transport_id_index", columnList = "transport_id"),
        @Index(name = "deliver_index", columnList = "id")
})
public class Deliver extends BaseEntity {
    @OneToMany(mappedBy = "deliver")
    private Set<Order> orders;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "transport_id", nullable = false)
    private Transport transport;

    @NonNull
    private Long departureDateTime;
    @NonNull
    private Long arriveDateTime;
    @NonNull
    private String fromAddress;
    @NonNull
    private String toAddress;

    public Set<Order> getOrders() {
        return orders;
    }

    public Long getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Long departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public Long getArriveDateTime() {
        return arriveDateTime;
    }

    public void setArriveDateTime(Long arriveDateTime) {
        this.arriveDateTime = arriveDateTime;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }
}
