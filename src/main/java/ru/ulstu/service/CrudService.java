package ru.ulstu.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ulstu.model.shop.Cart;
import ru.ulstu.model.shop.Order;
import ru.ulstu.model.shop.Product;
import ru.ulstu.model.shop.User;
import ru.ulstu.model.transport.company.Deliver;
import ru.ulstu.model.transport.company.Driver;
import ru.ulstu.model.transport.company.Transport;
import ru.ulstu.service.shop.CartCrudService;
import ru.ulstu.service.shop.OrderCrudService;
import ru.ulstu.service.shop.ProductCrudService;
import ru.ulstu.service.shop.UserCrudService;
import ru.ulstu.service.transport.company.DeliverCrudService;
import ru.ulstu.service.transport.company.DriverCrudRepository;
import ru.ulstu.service.transport.company.TransportCrudRepository;

import java.time.Instant;
import java.util.*;

@Service
public class CrudService {
    private static final Logger log = LoggerFactory.getLogger(CrudService.class);

    private final static int PAGE_OF_RECORDS_SIZE = 20;
    private final CartCrudService cartCrudService;
    private final OrderCrudService orderCrudService;
    private final ProductCrudService productCrudService;
    private final UserCrudService userCrudService;
    private final DeliverCrudService deliverCrudService;
    private final DriverCrudRepository driverCrudRepository;
    private final TransportCrudRepository transportCrudRepository;


    public CrudService(
            CartCrudService cartCrudService,
            OrderCrudService orderCrudService,
            ProductCrudService productCrudService,
            UserCrudService userCrudService,
            DeliverCrudService deliverCrudService,
            DriverCrudRepository driverCrudRepository,
            TransportCrudRepository transportCrudRepository) {
        this.cartCrudService = cartCrudService;
        this.orderCrudService = orderCrudService;
        this.productCrudService = productCrudService;
        this.userCrudService = userCrudService;
        this.deliverCrudService = deliverCrudService;
        this.driverCrudRepository = driverCrudRepository;
        this.transportCrudRepository = transportCrudRepository;
    }

    public User createUser(String firstName, String lastName, String address, String email) {
        return createUser(firstName, lastName, address, email, null);
    }

    public User createUser(String firstName, String lastName, String address, String email, String phone) {
        log.info("Create user:\n");
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAddress(address);
        user.setEmail(email);
        user.setPhone(phone);
        log.info(user.toString());
        return userCrudService.create(user);
    }

    public Product createProduct(String title, Integer price, String description) {
        log.info("Create product:\n");
        Product product = new Product();
        product.setTitle(title);
        product.setPrice(price);
        product.setDescription(description);
        log.info(product.toString());
        return productCrudService.create(product);
    }

    public Cart createCart(User user, Product product, Integer amount) {
        return createCart(user, product, amount, null);
    }

    public Cart createCart(User user, Product product, Integer amount, String promoCode) {
        log.info("Create cart:\n");
        if (user == null) {
            throw new RuntimeException("User mustn't be empty");
        }
        if (user.getId() == null) {
            throw new RuntimeException("User id mustn't be null");
        }
        if (product == null) {
            throw new RuntimeException("Product mustn't be empty");
        }
        if (product.getId() == null) {
            throw new RuntimeException("Product id mustn't be null");
        }
        Cart cart = new Cart();
        cart.setUser(user);
        cart.setProduct(product);
        cart.setAmount(amount);
        cart.setPromoCode(promoCode);
        log.info(cart.toString());
        return cartCrudService.create(cart);
    }

    public Deliver createDeliver(Transport transport, Long departureDateTime, Long arriveDateTime, String fromAddress, String toAddress) {
        log.info("Create deliver:\n");
        if (transport == null) {
            throw new RuntimeException("Transport mustn't be empty");
        }
        if (transport.getId() == null) {
            throw new RuntimeException("Transport id mustn't be null");
        }
        Deliver deliver = new Deliver();
        deliver.setTransport(transport);
        deliver.setDepartureDateTime(departureDateTime);
        deliver.setArriveDateTime(arriveDateTime);
        deliver.setFromAddress(fromAddress);
        deliver.setToAddress(toAddress);
        log.info(deliver.toString());
        return deliverCrudService.create(deliver);
    }

    public Order createOrder(Long orderDateTime, Long arrivedDateTime, String toAddress, Boolean isHomeDelivery) {
        return createOrder(orderDateTime, arrivedDateTime, toAddress, isHomeDelivery, null);
    }

    public Order createOrder(Long orderDateTime, Long arrivedDateTime, String toAddress, Boolean isHomeDelivery, Deliver deliver) {
        log.info("Create order:\n");
        if (deliver != null && deliver.getId() == null) {
            throw new RuntimeException("Deliver id mustn't be null");
        }
        Order order = new Order();
        order.setOrderDateTime(orderDateTime);
        order.setArrivedDateTime(arrivedDateTime);
        order.setToAddress(toAddress);
        order.setHomeDelivery(isHomeDelivery);
        order.setDeliver(deliver);
        log.info(order.toString());
        return orderCrudService.create(order);
    }

    public Driver createDriver(String firstName, String lastName, String passport) {
        log.info("Create driver:\n");
        Driver driver = new Driver();
        driver.setFirstName(firstName);
        driver.setLastName(lastName);
        driver.setPassport(passport);
        log.info(driver.toString());
        return driverCrudRepository.create(driver);
    }

    public Transport createTransport(Driver driver, String carNumber, String carModelName) {
        log.info("Create driver:\n");
        if (driver == null) {
            throw new RuntimeException("Driver mustn't be empty");
        }
        if (driver.getId() == null) {
            throw new RuntimeException("Driver id mustn't be null");
        }
        Transport transport = new Transport();
        transport.setDriver(driver);
        transport.setCarNumber(carNumber);
        transport.setCarModelName(carModelName);
        log.info(transport.toString());
        return transportCrudRepository.create(transport);
    }


    public User updateUser(User user) {
        log.info("Update user:\n");
        if (user == null) {
            throw new RuntimeException("User mustn't be empty");
        }
        if (user.getId() == null) {
            throw new RuntimeException("User id mustn't be null");
        }
        log.info(user.toString());
        return userCrudService.update(user);
    }

    public Product updateProduct(Product product) {
        log.info("Update product:\n");
        if (product == null) {
            throw new RuntimeException("Product mustn't be empty");
        }
        if (product.getId() == null) {
            throw new RuntimeException("Product id mustn't be null");
        }
        log.info(product.toString());
        return productCrudService.update(product);
    }

    public Cart updateCart(Cart cart) {
        log.info("Update cart:\n");
        if (cart == null) {
            throw new RuntimeException("Cart mustn't be empty");
        }
        if (cart.getId() == null) {
            throw new RuntimeException("Cart id mustn't be null");
        }
        if (cart.getUser() == null) {
            throw new RuntimeException("User mustn't be empty");
        }
        if (cart.getUser().getId() == null) {
            throw new RuntimeException("User id mustn't be null");
        }
        if (cart.getProduct() == null) {
            throw new RuntimeException("Product mustn't be empty");
        }
        if (cart.getProduct().getId() == null) {
            throw new RuntimeException("Product id mustn't be null");
        }
        log.info(cart.toString());
        return cartCrudService.update(cart);
    }

    public Deliver updateDeliver(Deliver deliver) {
        log.info("Update deliver:\n");
        if (deliver == null) {
            throw new RuntimeException("Deliver mustn't be empty");
        }
        if (deliver.getId() == null) {
            throw new RuntimeException("Deliver id mustn't be null");
        }
        if (deliver.getTransport() == null) {
            throw new RuntimeException("Transport mustn't be empty");
        }
        if (deliver.getTransport().getId() == null) {
            throw new RuntimeException("Transport id mustn't be null");
        }
        log.info(deliver.toString());
        return deliverCrudService.update(deliver);
    }

    public Order updateOrder(Order order) {
        log.info("Update order:\n");
        if (order == null) {
            throw new RuntimeException("Order mustn't be empty");
        }
        if (order.getId() == null) {
            throw new RuntimeException("Order id mustn't be null");
        }
        if (order.getDeliver() != null && order.getDeliver().getId() == null) {
            throw new RuntimeException("Deliver id mustn't be null");
        }
        log.info(order.toString());
        return orderCrudService.update(order);
    }

    public Driver updateDriver(Driver driver) {
        log.info("Update driver:\n");
        if (driver == null) {
            throw new RuntimeException("Driver mustn't be empty");
        }
        if (driver.getId() == null) {
            throw new RuntimeException("Driver id mustn't be null");
        }
        log.info(driver.toString());
        return driverCrudRepository.update(driver);
    }

    public Transport updateTransport(Transport transport) {
        log.info("Update transport:\n");
        if (transport == null) {
            throw new RuntimeException("Transport mustn't be empty");
        }
        if (transport.getId() == null) {
            throw new RuntimeException("Transport id mustn't be null");
        }
        if (transport.getDriver() == null) {
            throw new RuntimeException("Driver mustn't be empty");
        }
        if (transport.getDriver().getId() == null) {
            throw new RuntimeException("Driver id mustn't be null");
        }
        log.info(transport.toString());
        return transportCrudRepository.update(transport);
    }


    public void deleteUser(User user) {
        log.info("Delete user\n");
        if (user == null) {
            throw new RuntimeException("User mustn't be empty");
        }
        if (user.getId() == null) {
            throw new RuntimeException("User id mustn't be null");
        }
        userCrudService.delete(user);
    }

    public void deleteProduct(Product product) {
        log.info("Delete product\n");
        if (product == null) {
            throw new RuntimeException("Product mustn't be empty");
        }
        if (product.getId() == null) {
            throw new RuntimeException("Product id mustn't be null");
        }
        productCrudService.delete(product);
    }

    public void deleteCart(Cart cart) {
        log.info("Delete cart\n");
        if (cart == null) {
            throw new RuntimeException("Cart mustn't be empty");
        }
        if (cart.getId() == null) {
            throw new RuntimeException("Cart id mustn't be null");
        }
        cartCrudService.delete(cart);
    }

    public void deleteDeliver(Deliver deliver) {
        log.info("Delete deliver\n");
        if (deliver == null) {
            throw new RuntimeException("Deliver mustn't be empty");
        }
        if (deliver.getId() == null) {
            throw new RuntimeException("Deliver id mustn't be null");
        }
        deliverCrudService.delete(deliver);
    }

    public void deleteOrder(Order order) {
        log.info("Delete order\n");
        if (order == null) {
            throw new RuntimeException("Order mustn't be empty");
        }
        if (order.getId() == null) {
            throw new RuntimeException("Order id mustn't be null");
        }
        orderCrudService.delete(order);
    }

    public void deleteDriver(Driver driver) {
        log.info("Delete driver\n");
        if (driver == null) {
            throw new RuntimeException("Driver mustn't be empty");
        }
        if (driver.getId() == null) {
            throw new RuntimeException("Driver id mustn't be null");
        }
        driverCrudRepository.delete(driver);
    }

    public void deleteTransport(Transport transport) {
        log.info("Delete transport\n");
        if (transport == null) {
            throw new RuntimeException("Transport mustn't be empty");
        }
        if (transport.getId() == null) {
            throw new RuntimeException("Transport id mustn't be null");
        }
        transportCrudRepository.delete(transport);
    }


    public void showAllRecords() {
        log.info("All users:\n");
        log.info(userCrudService.findAll().toString());
        log.info("All products:\n");
        log.info(productCrudService.findAll().toString());
        log.info("All carts:\n");
        log.info(cartCrudService.findAll().toString());
        log.info("All delivers:\n");
        log.info(deliverCrudService.findAll().toString());
        log.info("All orders:\n");
        log.info(orderCrudService.findAll().toString());
        log.info("All drivers:\n");
        log.info(driverCrudRepository.findAll().toString());
        log.info("All transports:\n");
        log.info(transportCrudRepository.findAll().toString());
    }

    public void showFirstPageOfRecords() {
        log.info("First page of users:\n");
        log.info(userCrudService.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of products:\n");
        log.info(productCrudService.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of carts:\n");
        log.info(cartCrudService.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of delivers:\n");
        log.info(deliverCrudService.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of orders:\n");
        log.info(orderCrudService.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of drivers:\n");
        log.info(driverCrudRepository.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
        log.info("First page of transports:\n");
        log.info(transportCrudRepository.findAll(0, PAGE_OF_RECORDS_SIZE).getItems().toString());
    }


    public void showStatistic() {
        log.info("Statistic:\n");
        List<Deliver> delivers = deliverCrudService.findAll();
        showDelivers(delivers);
    }

    public void showFilteredRecords() {
        log.info("Filtered records:\n");
        List<Deliver> delivers = deliverCrudService.findAllByArrivedAfter(Instant.now().toEpochMilli());
        showDelivers(delivers);
    }

    private void showDelivers(List<Deliver> delivers) {
        for (Deliver deliver : delivers) {
            Transport transport = deliver.getTransport();
            Driver driver = transport.getDriver();
            log.info(
                driver.getFirstName() + " " +
                driver.getLastName() + " " +
                transport.getCarNumber() + " " +
                deliver.getFromAddress() + " " +
                deliver.getToAddress() + " " +
                new Date(deliver.getDepartureDateTime()).toString() + " " +
                new Date(deliver.getArriveDateTime()).toString()
            );
        }
    }
}
