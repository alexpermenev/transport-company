package ru.ulstu.service.transport.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.transport.company.Driver;
import ru.ulstu.repository.transport.company.DriverRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class DriverCrudRepository implements Crud<Driver> {
    private final DriverRepository driverRepository;

    public DriverCrudRepository(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @Override
    public Driver create(Driver driver) {
        return driverRepository.saveAndFlush(driver);
    }

    @Override
    public List<Driver> findAll() {
        return driverRepository.findAll();
    }

    @Override
    public Driver get(Integer id) {
        return driverRepository.getOne(id);
    }

    @Override
    public Optional<Driver> find(Integer id) {
        return driverRepository.findById(id);
    }

    @Override
    public PageableItems<Driver> findAll(int offset, int count) {
        final Page<Driver> page = driverRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Driver update(Driver driver) {
        return driverRepository.saveAndFlush(driver);
    }

    @Override
    public void delete(Driver driver) {
        driverRepository.delete(driver);
    }
}
