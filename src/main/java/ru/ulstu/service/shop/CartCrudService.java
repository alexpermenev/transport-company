package ru.ulstu.service.shop;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.shop.Cart;
import ru.ulstu.repository.shop.CartRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class CartCrudService implements Crud<Cart> {
    private final CartRepository cartRepository;

    public CartCrudService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public Cart create(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public List<Cart> findAll() {
        return cartRepository.findAll();
    }

    @Override
    public Cart get(Integer id) {
        return cartRepository.getOne(id);
    }

    @Override
    public Optional<Cart> find(Integer id) {
        return cartRepository.findById(id);
    }

    @Override
    public PageableItems<Cart> findAll(int offset, int count) {
        final Page<Cart> page = cartRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Cart update(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public void delete(Cart cart) {
        cartRepository.delete(cart);
    }
}
