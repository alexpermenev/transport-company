package ru.ulstu.service.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.shop.Product;
import ru.ulstu.repository.shop.ProductRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class ProductCrudService implements Crud<Product> {
    private final ProductRepository productRepository;

    public ProductCrudService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product create(Product product) {
        return productRepository.saveAndFlush(product);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product get(Integer id) {
        return productRepository.getOne(id);
    }

    @Override
    public Optional<Product> find(Integer id) {
        return productRepository.findById(id);
    }

    @Override
    public PageableItems<Product> findAll(int offset, int count) {
        final Page<Product> page = productRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Product update(Product product) {
        return productRepository.saveAndFlush(product);
    }

    @Override
    public void delete(Product product) {
        productRepository.delete(product);
    }
}
